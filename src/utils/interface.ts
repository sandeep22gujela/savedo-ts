export interface IIssueDetail { // I(Interface)-Issue-Detail
    title?:string,
    comments:number,
    issueNumber?:number
    createdAt?:string,
    userName?:string
}