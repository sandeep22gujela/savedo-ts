import { IIssueDetail } from "./interface";

export const getDate = (date: string): string => {
    if (typeof date === 'string') {
        var jsDate = new Date(date);
        return ` ${jsDate.getDate()}/${jsDate.getMonth()}/${jsDate.getFullYear()}`
    }
    return 'sometime'
}


export const jsonParse = (text: string): any => {
    var jsonData;
    try {
        jsonData = JSON.parse(text)
    }
    catch (e) {
        console.exception(e)
        return {}
    }
    return jsonData
}

export const transformIssueList = (issues: any[] = []): IIssueDetail[] => {
    if (issues.length === 0) {
        console.log('issues', issues)
        return []
    }

    var transformedIssueList: IIssueDetail[] = [];
    transformedIssueList = issues.map((data = {}) => {
        return {
            'comments': data.comments || 0 ,
            'createdAt': data.created_at || '',
            'issueNumber': data.number || null,
            'title': data.title || '',
            'userName': data.user.login || ''
        }

    })

    return transformedIssueList
}