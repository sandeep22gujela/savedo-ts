import React, { Component } from 'react';
import './Issues-list.css'
import IssueItem from '../issue-item/Issue-Item'
import { APP_URL } from '../../utils/constants';
import { IIssueDetail } from '../../utils/interface';

interface Props {
  issuesList: IIssueDetail[]
}

interface State { }

class IssuesList extends Component<Props, State> {
  constructor(props: Props, state: State) {
    super(props, state)
  }

  getIssuesList() {
    const { issuesList } = this.props
    return issuesList.map((issue, index) => {
      return <IssueItem key={index} issue={issue} />
    })

  }

  getPageTitle(){
    const urlArray=APP_URL.split('/')
    return <div className='mt-20 color-primary'>{`${urlArray[4]}/`}<b>{`${urlArray[5]}`}</b></div>
  }

  render() {
    return (
      <div className='issues-list-container'>
        {this.getPageTitle()}
        <ul className='issues-list'>
          {this.getIssuesList()}
        </ul>
      </div>
    )
  }
}

export default IssuesList;
