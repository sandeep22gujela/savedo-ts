import React, { Component } from 'react';
import './Issue-Item.css'
import IssuesList from '../issues-list/Issues-list';
import { getDate } from '../../utils/helpers';
import { IIssueDetail } from '../../utils/interface';
interface Props {
  issue: IIssueDetail
}
interface State { }

class IssueItem extends Component<Props, State> {
  constructor(props: Props, state: State) {
    super(props, state)
  }

  render() {
    const { issue } = this.props
    console.log('issue',issue)
    return (
      <li className='issues-item-container'>
        <div className='title-wrapper'>
          <div className='title'>{issue.title}</div>
          <div className='comments'>{issue.comments > 0 ? `${issue.comments} Comments` : null}</div>
        </div>
        <div className='meta-wrapper'>
          <div className=''>#{issue.issueNumber} </div>
          <div className=''>Opened on {getDate(issue.createdAt || '' )}</div>
          <div className=''>By {issue.userName} </div>
        </div>
      </li>
    )
  }
}

export default IssueItem;
