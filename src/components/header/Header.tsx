import React, { Component } from 'react';
import './Header.css';
import logo from '../../assets/img/logo.png';

interface Props { }
interface State { }

class Header extends Component<Props, State> {
  constructor(props: Props, state: State) {
    super(props, state)
  }

  render() {
    return (
      <header className='header'>
        <img src={logo} alt='github' className='logo' />
      </header>
    )
  }
}

export default Header;
