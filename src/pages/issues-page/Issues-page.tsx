import React, { Component } from 'react';
import IssuesList from '../../components/issues-list/Issues-list';
import { APP_URL } from '../../utils/constants';
import { jsonParse, transformIssueList } from '../../utils/helpers';
import { IIssueDetail } from '../../utils/interface';

interface Props { }
interface State {
  issuesList: string[]
}

class IssuesPage extends Component<Props, State>  {
  constructor(props: Props, state: State) {
    super(props, state)
    this.state = {
      issuesList: []
    }
  }

  componentDidMount() {
    this.fetchIssuesList()
  }

  fetchIssuesList() {
    // Note: Not using any library for making Ajax request as just only one request is to be made.
    var that = this;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
      if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
        that.setState({
          issuesList: jsonParse(xmlHttp.responseText)
        })
    }
    xmlHttp.open("GET", APP_URL);
    xmlHttp.setRequestHeader("Accept", "application/vnd.github.v3.text+json");
    xmlHttp.send();
  }

  render() {
    const list = this.state.issuesList
    var issuesList :IIssueDetail[]
    issuesList = transformIssueList(list)
    return (
      <IssuesList issuesList={issuesList} />
    );
  }
}

export default IssuesPage;
