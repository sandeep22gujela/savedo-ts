import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import IssuesPage from '../issues-page/Issues-page';
import Header from '../../components/header/Header';

class App extends Component {
  render() {
    return <div>
      <Header />
      <IssuesPage />
    </div>

  }
}

export default App;
